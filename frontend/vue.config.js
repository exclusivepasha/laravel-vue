/* eslint-disable */
const webpack = require('webpack')
const Terser = require('terser-webpack-plugin')
const credential = require('fs').readFileSync('.credential', 'utf8')


const app = {
  publicPath: '/',
  chainWebpack: config => {
    config.resolve.alias
      .set('@ant-design/icons/lib/dist$', __dirname + '/src/lib/ant-design/icons.js');

    config.module.rule('svg').uses.clear();

    config.module.rule('svg')
      .test(/(assets\/svg-inline).+\.svg$/)
      .use('file-loader')
      .loader('svg-sprite-loader');

    config.module.rule('svgi')
      .test(/(assets(?!\/svg-inline)).+\.svg$/)
      .use('file-loader')
      .loader('file-loader')
      .tap(options => Object.assign(options || {}, { name: 'img/[name].[hash:8].[ext]' }));
  },
  devServer: {
    proxy: 'http://laravel.vue',
    public: 'http://laravel.vue:8080',
    overlay: { warnings: true, errors: true },
    headers: { 'Access-Control-Allow-Origin': '*' },
    disableHostCheck: true,
    inline: true,
    hot: true
  },
  css: {
    loaderOptions: {
      less: {
        lessOptions: {
          javascriptEnabled: true,
          modifyVars: {
            // 'font-family': 'Poppins, Tahoma, sans-serif',
            // 'primary-color': '#272755',
            // 'btn-danger-bg': '#EB0745',
            // 'link-color': '#272755',
            // 'input-color': '#272755',
            // 'input-placeholder-color': '#9393AA',
            // 'input-height-base': '38px',
            // 'btn-height-base': '40px',
            // 'font-size-base': '16px',
            // 'rate-star-color': '#FFC400',
            // 'radio-dot-color': '#000',
            // 'border-color-base': 'transparent',
            // 'padding-sm': '17px',
            // 'btn-border-width': '0px',
          }
        }
      }
    }
  }

}

if (process.env.NODE_ENV === 'production') {
  app.outputDir = '../public/'
  app.assetsDir = 'assets'
  app.indexPath = __dirname + '/../resources/views/app.blade.php'
  app.productionSourceMap = false
  app.configureWebpack = {
    // optimization: { splitChunks: false },
    performance: {
      hints: false
    },
    plugins: [
      new Terser({
        parallel: true,
        terserOptions: {
          compress: { drop_console: true },
          output: { comments: false }
        }
      }),
      new webpack.BannerPlugin(credential)
    ]
  };
  /*
  app.pwa = {
    manifestOptions: {
      name: "Laravel",
      short_name: "Laravel",
      theme_color: "#000000",
      background_color: "#ffffff",
      start_url: "/",
      display: "standalone",
      icons: [{ "src": "/img/logo.png", "sizes": "512x512", "type": "image/png" }]
    },
    themeColor: "#000000",
    iconPaths: {
      favicon32: 'img/logo.png',
      favicon16: 'img/logo.png',
      appleTouchIcon: 'img/logo.png',
      maskIcon: 'img/logo.png',
      msTileImage: 'img/logo.png',
    },
    appleMobileWebAppCapable: "yes",
    appleMobileWebAppStatusBarStyle: "default",
    workboxPluginMode: 'InjectManifest',
    workboxOptions: { swSrc: './service-worker.js' }
  }
  */
}

module.exports = app
