import http from '@/lib/axios'
import { yup, validator } from '@/utils/validator'

const form = {
  id: yup.string().trim().required(),
  value: yup.string().trim().required()
}


export const apiGetOptions = () => {
  return http.get('options')
}

export const apiGetOption = (id) => {
  return validator({ id }, {
    id: form.id
  }).then(() => http.get(`options/${id}`))
}

export const apiCreateOption = (id, value) => {
  return validator({ id, value }, {
    id: form.id,
    value: form.value
  }).then(() => http.post('options', { id, value }))
}

export const apiUpdateOption = (id, value) => {
  return validator({ id, value }, {
    id: form.id,
    value: form.value
  }).then(() => http.put(`options/${id}`, { value }))
}

export const apiDeleteOption = (id) => {
  return validator({ id }, {
    id: form.id
  }).then(() => http.delete(`options/${id}`))
}
