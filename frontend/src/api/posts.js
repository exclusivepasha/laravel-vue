import http from '@/lib/axios'

export const apiGetPosts = (params = {}) => {
  return http.get('posts', { params })
}

export const apiGetPost = (id) => {
  return http.get(`posts/${id}`)
}

export const apiCreatePost = (params) => {
  return http.post('posts', params)
}

export const apiUpdatePost = (id, params) => {
  return http.put(`posts/${id}`, params)
}

export const apiDeletePost = (id) => {
  return http.delete(`posts/${id}`)
}
