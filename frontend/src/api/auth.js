import http from '@/lib/axios'
import { fields, validator } from '@/utils/validator'


export const apiInit = () => {
  return http.get('init')
}

export const apiGetAuth = () => {
  return http.get('auth')
}

export const apiSignIn = (params) => {
  return validator(params, {
    email: fields.email,
    password: fields.password
  }).then(() => http.post('auth/sign-in', params))
}

export const apiSignUp = (params) => {
  return validator(params, {
    email: fields.email,
    name: fields.name,
    password: fields.password,
    password_confirmation: fields.password_confirmation
  }).then(() => http.post('auth/sign-up', params))
}

export const apiUpdateAuth = (email) => {
  return validator({ email }, {
    email: fields.email
  }).then(() => http.post('auth/data', { email }))
}

export const apiUploadLogo = (fd) => {
  return http.post('auth/logo', fd)
}

export const apiDeleteLogo = () => {
  return http.delete('auth/logo')
}

export const apiLogout = () => {
  return http.delete('auth')
}
