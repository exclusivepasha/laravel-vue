import http from '@/lib/axios'

export const apiGetUsers = (params = {}) => {
  return http.get('users', { params })
}

export const apiGetUser = (id) => {
  return http.get(`users/${id}`)
}

export const apiCreateUser = (params) => {
  return http.post('users', params)
}

export const apiUpdateUser = (id, params) => {
  return http.put(`users/${id}`, params)
}

export const apiDeleteUser = (id) => {
  return http.delete(`users/${id}`)
}
