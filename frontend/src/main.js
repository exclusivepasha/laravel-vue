import 'core-js/stable'
import 'regenerator-runtime/runtime'
import Vue from 'vue'
import VueBus from 'vue-bus'
import vClickOutside from 'v-click-outside'
import VueDebounce from 'vue-debounce'

import './style/app.sass'
import './config/filters'
import './config/prototypes'
import './utils/events'
import './lib/ant-design/'

import i18n from './lib/i18n/'
import store from './lib/store/'
import router from './lib/router/'

import ErrorMessage from './components/ErrorMessage'
import FormError from './components/FormError'
import InputCode from './components/InputCode'
import Modal from './components/Modal'
import InlineSvg from './components/InlineSvg'
import App from './App.vue'

import { apiInit } from '@/api/auth'



Vue.config.productionTip = false
Vue.use(VueBus)
Vue.use(vClickOutside)
Vue.use(VueDebounce, {
  cancelOnEmpty: true,
  defaultTime: '1s',
  listenTo: ['input', 'keyup']
})
Vue.component('ErrorMessage', ErrorMessage)
Vue.component('FormError', FormError)
Vue.component('InputCode', InputCode)
Vue.component('Modal', Modal)
Vue.component('InlineSvg', InlineSvg)


const files = require.context('./assets/svg-inline/', false, /\.svg$/)
files.keys().forEach(files)



const app = () => {
  new Vue({
    el: '#app',
    i18n,
    store,
    router,
    render: r => r(App)
  })
}

apiInit()
  .then(r => {
    store.dispatch('app/options', r.options)
    store.dispatch('auth/user', r.user).then(app)
  })
  .catch(() => {
    store.dispatch('auth/user').then(app)
  })
