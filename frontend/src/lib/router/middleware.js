import Vue from 'vue'
import { MAIN_ROUTE } from '@/config/const'


export const Auth = (next) => {
  if (typeof Vue.prototype.$user('id') === 'number') return true
  next({ name: 'sign_in' })
}


export const Guest = (next) => {
  if (Vue.prototype.$user() === false) return true
  next({ name: MAIN_ROUTE })
}


export const Role = (next, array) => {
  if (Vue.prototype.$isRole(array)) return true
  next({ name: MAIN_ROUTE })
}


export const Can = (next, array) => {
  if (Vue.prototype.$isCan(array)) return true
  next({ name: MAIN_ROUTE })
}
