import Home from '@/pages/Home'
import NotFound from '@/pages/404'
import Profile from '@/pages/Profile'
import { MAIN_ROUTE } from '@/config/const'

export default [{
  path: '/',
  name: MAIN_ROUTE,
  component: Home
}, {
  path: '/404',
  name: 'not_found',
  component: NotFound
}, {
  path: '/profile',
  name: 'profile',
  component: Profile
}, {
  path: '*',
  component: NotFound
}]

// { path: '/lazy-load', component: () =>  import(/* webpackChunkName: "lazy-load" */ "./view/LazyLoad") },
