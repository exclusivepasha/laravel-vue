import Vue from 'vue'
import axios from 'axios'
import { getToken } from '@/utils/localStorage'


const token = () => {
  const token = getToken()
  return token ? { Authorization: `Bearer ${token}` } : {}
}


const http = axios.create({
  baseURL: process.env.VUE_APP_API_URL + 'api/',
  headers: {
    Accept: 'application/json',
    'Access-Control-Allow-Origin': '*',
    'Content-Type': 'application/json',
    ...token()
  },
  transformRequest: (data, heads) => {
    const fd = data instanceof FormData
    heads['Content-Type'] = fd ? 'multipart/form-data' : 'application/json'
    return fd ? data : JSON.stringify(data || '{}')
  },
  transformResponse: data => data ? JSON.parse(data) : {}
})


http.interceptors.response.use(
  ({ data }) => {
    data.message && Vue.prototype.$msg(data.message, 1)
    return data
  },
  ({ response }) => {
    const data = Vue.prototype.$get(response, 'data', {})
    data.message && Vue.prototype.$msg(data.message, 0)

    const errors = {}
    const e = data.errors || {}
    for (const l in e) errors[l] = e[l][0]

    throw { ...response, errors }
  }
)


export default http
