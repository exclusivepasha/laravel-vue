import Vue from 'vue'

import {
  Button,
  Checkbox,
  Col,
  Dropdown,
  Input,
  message,
  Modal,
  Radio,
  Row,
  Select,
  Switch,
  Table
} from 'ant-design-vue'

import 'ant-design-vue/lib/style/index.less'
import 'ant-design-vue/lib/button/style/index.less'
import 'ant-design-vue/lib/checkbox/style/index.less'
import 'ant-design-vue/lib/dropdown/style/index.less'
import 'ant-design-vue/lib/grid/style/index.less'
import 'ant-design-vue/lib/input/style/index.less'
import 'ant-design-vue/lib/message/style/index.less'
import 'ant-design-vue/lib/modal/style/index.less'
import 'ant-design-vue/lib/radio/style/index.less'
import 'ant-design-vue/lib/select/style/index.less'
import 'ant-design-vue/lib/switch/style/index.less'
import 'ant-design-vue/lib/table/style/index.less'


Vue.use(Button)
Vue.use(Checkbox)
Vue.use(Col)
Vue.use(Dropdown)
Vue.use(Input)
Vue.use(Modal)
Vue.use(Radio)
Vue.use(Row)
Vue.use(Select)
Vue.use(Switch)
Vue.use(Table)

Vue.prototype.$confirm = Modal.confirm
Vue.prototype.$message = {
  error: message.error,
  success: message.success
}
