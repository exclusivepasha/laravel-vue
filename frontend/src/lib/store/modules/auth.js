import { apiGetAuth } from '@/api/auth'

export default {
  namespaced: true,

  state: {
    data: null
  },

  mutations: {
    user (state, value = false) {
      state.data = value
    }
  },

  actions: {
    user ({ commit }, value = false) {
      commit('user', this._vm.$isObject(value) ? value : false)
    },
    checkUser ({ commit }) {
      apiGetAuth()
        .then(r => {
          commit('user', r.data || false)
        })
        .catch(() => {
          commit('user')
        })
    }
  }
}
