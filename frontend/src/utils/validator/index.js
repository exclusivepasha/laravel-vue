import * as yup from 'yup'
import fields from './fields'


const validator = (data, object) => yup.object()
  .shape(object)
  .validate(data, { abortEarly: false })
  .catch(e => {
    const errors = {}
    e.inner.map(el => {
      errors[el.path] = el.message
    })
    throw { errors }
  })


export {
  validator,
  fields,
  yup
}
