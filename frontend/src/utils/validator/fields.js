import * as yup from 'yup'
import i18n from '@/lib/i18n'
import { IMAGE_SIZE, IMAGE_SIZE_MB } from '@/config/const'

export default {
  anyNumber: key => yup
    .number()
    .nullable()
    .required()
    .min(1)
    .label(i18n.t(key)),
  anyString: (key = 'field') => yup
    .string()
    .nullable()
    .required()
    .min(1)
    .max(255)
    .label(i18n.t(key)),
  anyDate: key => yup
    .date()
    .required()
    .label(i18n.t(key))
    .typeError(i18n.t('required_field', { type: i18n.t(key) })),
  file: yup
    .mixed()
    .nullable()
    .test(
      i18n.t('file'),
      i18n.t('invalid_file_size', { size: IMAGE_SIZE_MB }),
      file => file === null || (file && file.size <= IMAGE_SIZE)
    )
    .label(i18n.t('file')),
  field: yup
    .string()
    .nullable()
    .required()
    .min(1)
    .max(255)
    .label(i18n.t('field')),
  token: yup
    .string()
    .required()
    .max(255)
    .label(i18n.t('token')),
  email: yup
    .string()
    .required()
    .email()
    .max(255)
    .label(i18n.t('email_address')),
  password: yup
    .string()
    .required()
    .min(8)
    .max(32)
    .label(i18n.t('password')),
  password_confirmation: yup
    .string()
    .required()
    .min(8)
    .max(32)
    .oneOf([yup.ref('password')], i18n.t('confirm_password_do_not_match'))
    .label(i18n.t('confirm_password'))
}
