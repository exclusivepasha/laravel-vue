import { STORE_NAME } from '@/config/const'

const token_name = STORE_NAME + '_auth_token'


export function getToken () {
  return localStorage.getItem(token_name)
}

export function setToken (token) {
  localStorage.setItem(token_name, token)
}

export function removeToken () {
  localStorage.removeItem(token_name)
}
