export default {
  mounted () {
    document.body.classList.add('no-scroll')
  },
  beforeDestroy () {
    document.body.classList.remove('no-scroll')
  }
}
