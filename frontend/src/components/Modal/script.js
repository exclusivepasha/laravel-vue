export default {
  props: {
    title: {
      type: String,
      required: true
    },
    preventClose: {
      type: Boolean,
      default: false
    }
  },

  methods: {
    close (e) {
      if (e.target.closest('#app')) this.$emit('close', e)
    }
  }
}
