export default {
  props: {
    type: {
      type: String,
      default: 'tel'
    },
    length: {
      type: Number,
      default: 6
    },
    placeholder: {
      type: String,
      default: '-'
    },
    disabled: {
      type: Boolean,
      default: false
    }
  },

  data: () => ({
    value: ''
  }),

  methods: {
    input (e) {
      this.$emit('input', this.value)

      if (e.target.value.length >= this.length) {
        document.activeElement.blur()
        this.$refs.input.blur()
      }
    },
    clear () {
      this.value = ''
      this.$emit('input', '')
    }
  }
}
