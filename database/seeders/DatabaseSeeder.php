<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Database\Factories\{
    OptionFactory,
    PostFactory,
    FileFactory
};
use App\Models\{
    File,
    Option,
    Post
};

class DatabaseSeeder extends Seeder
{
    public function run()
    {
        $this->call(SeederUser::class);
        OptionFactory::factoryForModel(Option::class)->count(10)->create();
        PostFactory::factoryForModel(Post::class)->count(50)->create();
        FileFactory::factoryForModel(File::class)->count(10)->create();
    }
}
