<?php

namespace Database\Seeders;

use App\Enums\RoleEnum;
use Database\Factories\UserFactory;
use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;
use App\Models\User;

class SeederUser extends Seeder
{
    public function run()
    {
        foreach (RoleEnum::getKeys() as $key => $val) {
            Role::create(['name' => $key]);
        }

        foreach (RoleEnum::getKeys() as $key => $val) {
            User::create([
                'name' => $val,
                'email' => $key . '@example.com',
                'password' => 'Pass1234',
                'email_verified_at' => now()
            ])->syncRoles($key);
        }

        UserFactory::factoryForModel(User::class)->count(10)->create();
    }
}
