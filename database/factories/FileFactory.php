<?php

namespace Database\Factories;

use App\Enums\FileEnum;
use App\Models\File;
use Illuminate\Database\Eloquent\Factories\Factory;

class FileFactory extends Factory
{
    static $array = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];

    public function definition()
    {
        return [
            'title' => $this->faker->name,
            'path' => 'user-logo/' . $this->faker->numberBetween() .'.png',
            'type' => FileEnum::USER_LOGO,
            'created_at' => now(),
            'morphable_id' => array_shift(self::$array),
            'morphable_type' => File::class,
        ];
    }
}
