<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

class OptionFactory extends Factory
{
    public function definition()
    {
        $values = [$this->faker->slug , ['a' => $this->faker->slug, 'b' => $this->faker->slug]];
        return [
            'id' => $this->faker->unique()->word,
            'value' => $this->faker->randomElement($values),
            'init' => rand(0, 1)
        ];
    }
}
