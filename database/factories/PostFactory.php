<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

class PostFactory extends Factory
{
    public function definition()
    {
        $time = now()->addHours(rand(-72, 0));
        return [
            'user_id' => rand(2, 5),
            'title' => $this->faker->text(50),
            'content' => $this->faker->paragraphs(3, true),
            'created_at' => $time,
            'updated_at' => $time
        ];
    }
}
