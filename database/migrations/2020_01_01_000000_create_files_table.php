<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFilesTable extends Migration
{
    public function up()
    {
        Schema::create('files', function (Blueprint $table) {
            $table->id();
            $table->string('title')->nullable();
            $table->string('path');
            $table->timestamp('created_at')->useCurrent();
            $table->unsignedTinyInteger('type')->nullable();
            $table->morphs('morphable');
        });
    }

    public function down()
    {
        Schema::dropIfExists('files');
    }
}
