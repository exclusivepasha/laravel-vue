<?php

use App\Enums\RoleEnum;

Route::get('init', 'AppController@init');

Route::resource('options', 'Crud\OptionCrud')->only(['index', 'show']);
Route::resource('posts', 'Crud\PostCrud')->only(['index', 'show']);


Route::middleware('guest')->group(function () {
    Route::post('auth/sign-in', 'Auth\LoginController@signIn');
    Route::post('auth/sign-up', 'Auth\LoginController@signUp');
    Route::post('auth/resend-verification', 'Auth\LoginController@resendVerification');
    Route::post('auth/forgot', 'Auth\ResetController@forgot');
    Route::post('auth/reset', 'Auth\ResetController@reset');
});


Route::middleware('auth')->group(function () {
    Route::get('auth', 'Auth\AuthController@get');
    Route::delete('auth', 'Auth\AuthController@logout');
    Route::delete('auth/all', 'Auth\AuthController@logoutAll');
    Route::put('auth/data', 'Auth\DataController@update');
    Route::put('auth/password', 'Auth\DataController@password');
    Route::post('auth/logo', 'Auth\DataController@uploadLogo');
    Route::delete('auth/logo', 'Auth\DataController@deleteLogo');
});


Route::middleware('role:'. RoleEnum::MODERATOR)->group(function () {
    Route::resource('posts', 'Crud\PostCrud')->only(['store', 'update', 'destroy']);
});


Route::middleware('role:'. RoleEnum::ADMIN)->group(function () {
    Route::apiResource('users', 'Crud\UserCrud');
    Route::resource('options', 'Crud\OptionCrud')->only(['store', 'update', 'destroy']);
});
