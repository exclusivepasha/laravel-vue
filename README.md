Production:
```bash
composer production
php artisan migrate:fresh --seed
```

Echo:
```bash
npm install -g laravel-echo-server
laravel-echo-server init
laravel-echo-server start --dir=/project/folder
```

Supervisor:
```bash
sudo supervisorctl reread
sudo supervisorctl update
```
