<?php

return [
    'failed'                    => 'These credentials do not match our records.',
    'throttle'                  => 'Too many login attempts. Please try again in :seconds seconds.',
    'failed_current_password'   => 'Current password is incorrect',
    'forgot_emailed'            => 'We have e-mailed your password reset link',
    'logo_deleted'              => 'Logo successfully deleted',
    'logo_updated'              => 'Logo successfully uploaded',
    'reset_failed'              => 'This password reset token is invalid',
    'resend_done'               => 'We have e-mailed your verification link',
    'sign_in_failed'            => 'The provided credentials are incorrect',
    'sign_in_verify'            => 'Your account is not verified. Check your email address',
    'sign_up_failed'            => 'Unsuccessful registration. Try again',
    'sign_up_done'              => 'Your account successfully created. Check your email address',
    'user_updated'              => 'User information updated successfully',
    'verify_signature'          => 'Invalid Signature token',
    'verify_hash'               => 'Invalid Hash token'
];
