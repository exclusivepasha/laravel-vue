<?php

return [
    'admin'                 => 'Admin',
    'fill_in_all'           => 'Fill in all fields',
    'member'                => 'Member',
    'moderator'             => 'Moderator',
    'not_found'             => 'Not found',
    'unauthorized'          => 'Unauthorized',
    'upload_file_failed'    => 'Unknown error while uploading file. Try again',
    'order_by'              => 'Invalid `order_by` parameter',
    'order_dir'             => 'Invalid `order_by` direction',
];
