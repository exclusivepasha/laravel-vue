<?php

return [

    'url' => env('APP_FRONT_URL', 'http://localhost'),

    'recovery_url' => env('APP_FRONT_URL', 'http://localhost') . '/recovery-password',

    'date_format' => 'd/m/Y',

    'time_format' => 'g:i A',

    'date_time_format' => 'g:i A - d/m/Y'
];
