<?php

use Illuminate\Validation\Rules\Password;

$password = Password::min(8)->numbers()->mixedCase();

return [
    'email' => 'required|email:filter|max:100',
    'email_users_unique' => 'required|email:filter|max:100|unique:users,email',
    'image' => 'required|file|mimes:jpeg,jpg,png|max:10240|dimensions:min_width=100,min_height=100',
    'password' => ['required', 'max:32', $password],
    'password_confirmed' => ['required', 'confirmed', 'max:32', $password],
    'user_name' => 'required|string|max:100'
];
