<?php

namespace Tests\Feature\Controllers\Auth;

use Database\Factories\UserFactory;
use Tests\TestCase;
use Laravel\Sanctum\Sanctum;
use App\Models\User;

class AuthTest extends TestCase
{
    public function testInit()
    {
        $this->get('/api/init')->assertJsonStructure(['user', 'options']);
    }

    public function testGet()
    {
        $this->get('/api/auth')->assertStatus(401);

        Sanctum::actingAs(UserFactory::factoryForModel(User::class)->create());

        $this->get('/api/auth')->assertJsonStructure(['data']);
    }
}
