<?php

namespace Tests\Feature\Controllers\Auth;

use Database\Factories\UserFactory;
use Tests\TestCase;
use Laravel\Sanctum\Sanctum;
use App\Models\User;

class LoginTest extends TestCase
{
    public function testSignIn()
    {
        $user = UserFactory::factoryForModel(User::class)->create();

        $attempt = $this->post('api/auth/sign-in', [
            'email' => $user->email,
            'password' => 'Pass1234'
        ])->assertJsonStructure(['token', 'user']);

        Sanctum::actingAs($user);

        $this->get('api/auth', [
            'Authorization' => "Bearer {$attempt->json()['token']}"
        ])->assertJsonStructure(['data']);
    }

    public function testSignUp()
    {
        $user = UserFactory::factoryForModel(User::class)->make();

        $this->post('api/auth/sign-up', [
            'email' => $user->email,
            'password' => 'Pass1234',
            'password_confirmation' => 'Pass1234',
            'name' => $user->name,
        ])->assertStatus(200);

        $this->assertDatabaseHas('users', [
            'email' => $user->email,
            'name' => $user->name
        ]);
    }

    public function testResendVerification()
    {
        $user = UserFactory::factoryForModel(User::class)->make();
        $user->email_verified_at = null;
        $user->save();

        $this->post('api/auth/resend-verification', [
            'email' => $user->email,
        ])->assertStatus(200);
    }
}
