<?php

namespace Tests\Feature\Controllers\Auth;

use Database\Factories\UserFactory;
use Tests\TestCase;
use App\Jobs\ForgotPasswordJob;
use App\Models\User;

class ResetTest extends TestCase
{
    private static $user;

    public function testForgot()
    {
        self::$user = UserFactory::factoryForModel(User::class)->create();

        $this->post('/api/auth/forgot', ['email' => self::$user->email])
             ->assertStatus(200);

        $job = new ForgotPasswordJob(self::$user->email);
        $job->handle();

        $this->assertDatabaseHas('password_resets', ['email' => self::$user->email]);
    }
}
