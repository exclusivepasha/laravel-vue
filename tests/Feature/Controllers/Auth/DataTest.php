<?php

namespace Tests\Feature\Controllers\Auth;

use Database\Factories\UserFactory;
use Illuminate\Http\UploadedFile;
use Tests\TestCase;
use Laravel\Sanctum\Sanctum;
use App\Models\User;

class DataTest extends TestCase
{
    private static $user;
    private static $logo;
    private $email = 'test@test.test';
    private $name = 'New Name';
    private $password = 'NewPassword123';

    public function testIndex()
    {
        self::$user = UserFactory::factoryForModel(User::class)->create();
        Sanctum::actingAs(self::$user);

        $this->put('/api/auth/data', [
            'email' => $this->email,
            'name' => $this->name
        ])->assertStatus(200);


        $user = User::find(self::$user->id);
        $this->assertTrue($user->name == $this->name);
        $this->assertFalse($user->email == $this->email);
    }

    public function testPassword()
    {
        Sanctum::actingAs(self::$user);

        $this->put('/api/auth/password', [
            'current' => 'Pass1234',
            'password' => $this->password,
            'password_confirmation' => $this->password
        ])->assertStatus(200);

        $user = User::find(self::$user->id);
        $this->assertTrue(\Hash::check($this->password, $user->password));
    }

    public function testUpload()
    {
        Sanctum::actingAs(self::$user);

        $this->post('/api/auth/logo', ['logo' => UploadedFile::fake()->image('Test Logo.png', 1000, 500)])
             ->assertStatus(200);

        self::$logo = User::find(self::$user->id)->logo->path;

        $this->assertNotNull(self::$logo);

        $this->assertTrue(\Storage::disk('public')->exists(self::$logo));
    }

    public function testDelete()
    {
        Sanctum::actingAs(self::$user);

        $this->delete('/api/auth/logo')
             ->assertStatus(200);

        $this->assertNull(User::find(self::$user->id)->logo);

        $this->assertFalse(\Storage::disk('public')->exists(self::$logo));
    }
}
