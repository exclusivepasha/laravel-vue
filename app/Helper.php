<?php

namespace App;

class Helper
{
    /**
     * Get full url from File model
     *
     * @param $file - MorphOne|MorphMany
     * @return array|string|null
     */
    public static function getFileUrl($file)
    {
        if (is_countable($file)) { # collection
            return $file->map(function ($item) {
                return \Storage::url($item['path']);
            });
        } else { # single file
            return $file ? \Storage::url($file->path) : null;
        }
    }

    /**
     * Return validation error format (as in FormRequest)
     *
     * @param string $key
     * @param string $message
     * @return \Illuminate\Http\JsonResponse
     */
    public static function failedField(string $key, string $message)
    {
        return response()->json(['errors' => [$key => [$message]]], 422);
    }

}
