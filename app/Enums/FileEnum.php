<?php

namespace App\Enums;

class FileEnum
{
    const USER_LOGO = 1;
    const USER_GALLERY = 2;

    public static function path(int $type, string $slug = '') : string
    {
        switch ($type) {
            case self::USER_LOGO:
                return 'user-logo'. DIRECTORY_SEPARATOR . $slug;
            case self::USER_GALLERY:
                return 'user-gallery' . DIRECTORY_SEPARATOR . date('Y-m') . DIRECTORY_SEPARATOR . $slug;
            default:
                return 'temp' . DIRECTORY_SEPARATOR . $slug;
        }
    }
}
