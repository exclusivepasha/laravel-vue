<?php

namespace App\Enums;

class RoleEnum
{
    const ADMIN = 'admin';
    const MODERATOR = 'moderator';
    const MEMBER = 'member';

    public static function getKeys()
    {
        return [
            self::ADMIN => __('app.' . self::ADMIN),
            self::MODERATOR => __('app.' . self::MODERATOR),
            self::MEMBER => __('app.' . self::MEMBER)
        ];
    }
}
