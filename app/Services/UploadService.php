<?php

namespace App\Services;

use App\Enums\FileEnum;
use App\Models\File;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\MorphMany;
use Illuminate\Database\Eloquent\Relations\MorphOneOrMany;
use Illuminate\Http\UploadedFile;
use Intervention\Image\ImageManager;
use Symfony\Component\HttpFoundation\File\Exception\UploadException;

/**
 * @param UploadedFile      $file - file from request
 * @param int               $type - file type (const from FileEnum)
 * @param MorphOneOrMany    $relation - for example: $user->logo()
 * @param string            $path - folder path from storage root
 * @param string            $name - custom name
 * @param string            $ext - file extension
 * @param [int, ?int]       $resize - new size [width, height]
 * @return ?Model
 */
class UploadService
{
    public $file;
    public $type;
    public $relation;
    public $path;
    public $name;
    public $ext;
    public $resize;

    public function setFile(UploadedFile $file) : UploadService
    {
        $this->file = $file;

        return $this;
    }

    public function setType(int $type) : UploadService
    {
        $this->type = $type;

        return $this;
    }

    public function setRelation(MorphOneOrMany $relation) : UploadService
    {
        $this->relation = $relation;

        return $this;
    }

    public function setPath(string $path) : UploadService
    {
        $this->path = $path;

        return $this;
    }

    public function setName(string $name) : UploadService
    {
        $this->name = $name;

        return $this;
    }

    public function setResize(int $w, ?int $h = null) : UploadService
    {
        $this->resize = [$w, $h];

        return $this;
    }

    public function upload() : ?Model
    {
        $this->checkFile();
        $this->checkPath();
        $this->checkName();

        return $this->store();
    }

    private function checkFile()
    {
        if (! isset($this->file, $this->type, $this->relation)) {
            throw new UploadException('Error occurred during file upload');
        }
    }

    private function checkPath()
    {
        if (! $this->path) {
            $this->path = FileEnum::path($this->type);
        }
    }

    private function checkName()
    {
        if (! $this->name) {
            $this->name = time() . \Str::random();
        }

        $this->path .= $this->name .'.'. $this->file->getClientOriginalExtension();
    }

    private function resize() : void
    {
        \Storage::makeDirectory(pathinfo($this->path)['dirname']);
        $fn = ($this->resize[0] and $this->resize[1]) ? 'fit' : 'resize';

        (new ImageManager)
            ->make($this->file)
            ->$fn($this->resize[0], $this->resize[1], function ($constraint) {
                if (! $this->resize[0] or ! $this->resize[1]) {
                    $constraint->aspectRatio();
                }
                $constraint->upsize();
            })
            ->save(\Storage::path($this->path));
    }

    private function store() : ?Model
    {
        if ($this->resize) {
            try {
                $this->resize();
            } catch (\Exception $e) {
                return null;
            }
        } else {
            if (! \Storage::put($this->path, file_get_contents($this->file))) {
                return null;
            }
        }

        return $this->model();
    }

    private function model() : Model
    {
        $model = new File;
        $model->title = substr(pathinfo($this->file->getClientOriginalName(), PATHINFO_FILENAME) ?? '', 0, 255);
        $model->path = $this->path;
        $model->type = $this->type;
        $model->created_at = now();

        if ($this->relation instanceof MorphMany) {
            return $this->relation->create($model->toArray());
        } else {
            return $this->relation->updateOrCreate(
                ['id' => data_get($this->relation->first(), 'id')],
                $model->toArray()
            );
        }
    }
}
