<?php

namespace App\Http\Controllers\Crud;

use App\Http\Requests\PaginateFr;
use App\Http\Requests\Crud\UserCrudFr;
use App\Models\User;

class UserCrud
{
    public function index(PaginateFr $fr, User $user)
    {
        return $user
            ->orderBy(request('order_by', 'id'), request('order_dir', 'asc'))
            ->paginate(request('per_page', 10));
    }


    public function show(User $user)
    {
        return $user;
    }


    public function store(UserCrudFr $fr, User $user)
    {
        $user = $user->fill(request()->input());
        $user->save();
        return $user;
    }


    public function update(UserCrudFr $fr, User $user)
    {
        $user = $user->fill(request()->input());
        $user->save();
        return $user;
    }


    public function destroy(User $user)
    {
        return $user->delete();
    }
}
