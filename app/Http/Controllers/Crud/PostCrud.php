<?php

namespace App\Http\Controllers\Crud;

use App\Http\Requests\Crud\PostCrudFr;
use App\Http\Requests\PaginateFr;
use App\Models\Post;

class PostCrud
{
    public function index(PaginateFr $fr, Post $post)
    {
        return $post
            ->with('user')
            ->orderBy(request('order_by', 'id'), request('order_dir', 'desc'))
            ->paginate(request('per_page', 10));
    }


    public function show(Post $post)
    {
        return $post->load('user');
    }


    public function store(PostCrudFr $fr, Post $post)
    {
        $post = $post->fill(request()->input());
        $post->user_id = auth()->id();
        $post->save();
        return $post;
    }


    public function update(PostCrudFr $fr, Post $post)
    {
        $post = $post->fill(request()->input());
        $post->save();
        return $post;
    }


    public function destroy(Post $post)
    {
        return $post->delete();
    }
}
