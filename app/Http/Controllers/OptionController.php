<?php

namespace App\Http\Controllers;

use App\Http\Requests\OptionFr;
use App\Models\Option;

class OptionController
{
    public function index()
    {
        return Option::init();
    }


    public function show(Option $option)
    {
        return $option;
    }


    public function store(OptionFr $fr, Option $option)
    {
        $option = $option->fill(request()->only(['id', 'value']));
        $option->save();
        return $option;
    }


    public function update(Option $option)
    {
        $option->update(['value' => request('value')]);
        return $option;
    }


    public function destroy(Option $option)
    {
        return $option->delete();
    }
}
