<?php

namespace App\Http\Controllers;

use App\Models\Option;
use App\Http\Resources\UserResource;

class AppController
{
    public function init()
    {
        return [
            'user' => new UserResource(auth()->user()),
            'options' => Option::init()
        ];
    }
}
