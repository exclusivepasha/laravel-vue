<?php

namespace App\Http\Controllers\Auth;

use App\Http\Resources\UserResource;

class AuthController
{
    public function get()
    {
        return new UserResource(auth()->user());
    }

    public function logout()
    {
        request()->user()->currentAccessToken()->delete();
        return [];
    }

    public function logoutAll()
    {
        request()->user()->tokens()->delete();
        return [];
    }
}
