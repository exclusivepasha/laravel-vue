<?php

namespace App\Http\Controllers\Auth;

use App\Enums\FileEnum;
use App\Http\Requests\Auth\DataFr;
use App\Http\Requests\Auth\LogoFr;
use App\Http\Requests\Auth\PasswordFr;
use App\Services\UploadService;

class DataController
{
    public function update(DataFr $fr)
    {
        $user = auth()->user();
        $user->fill(request()->except(['email', 'password']));
        $user->save();

        return ['message' => __('auth.user_updated')];
    }

    public function password(PasswordFr $fr)
    {
        $user = auth()->user();

        abort_if(! \Hash::check(request('current'), $user->password), 422, __('auth.failed_current_password'));

        $user->update(['password' => request('password')]);

        return ['message' => __('auth.password_updated')];
    }

    public function uploadLogo(LogoFr $fr, UploadService $service)
    {
        $file = $service
            ->setFile(request()->file('logo'))
            ->setType(FileEnum::USER_LOGO)
            ->setRelation(auth()->user()->logo())
            ->setResize(200, 200)
            ->upload();

        abort_if(is_null($file), 422, __('app.upload_file_failed'));

        return ['message' => __('auth.logo_updated')];
    }

    public function deleteLogo()
    {
        if (auth()->user()->logo) {
            auth()->user()->logo->delete();
        }

        return ['message' => __('auth.logo_deleted')];
    }
}
