<?php

namespace App\Http\Controllers\Auth;

use App\Helper;
use App\Http\Requests\Auth\EmailUserFr;
use App\Http\Requests\Auth\SignInFr;
use App\Http\Requests\Auth\PasswordFr;
use App\Http\Requests\Auth\SignUpFr;
use App\Http\Requests\EmailFr;
use App\Jobs\EmailVerificationJob;
use App\Models\User;
use App\Http\Resources\UserResource;

class LoginController
{
    public function signIn(SignInFr $fr)
    {
        $user = User::where('email', request('email'))->first();

        if (! $user || ! \Hash::check(request('password'), $user->password)) {
            return Helper::failedField('email', __('auth.sign_in_failed'));
        }

        if (! $user->email_verified_at) {
            dispatch(new EmailVerificationJob($user));

            return Helper::failedField('email', __('auth.sign_in_verify'));
        }

        return [
            'token' => $user->createToken(substr(request()->userAgent(), 0, 255))->plainTextToken,
            'user' => new UserResource($user)
        ];
    }

    public function signUp(SignUpFr $fr, User $user)
    {
        $user = $user->fill(request()->input());
        if (! request('name')) {
            $user->name = explode('@', request('email'))[0];
        }
        $user->save();

        if (! $user->id) {
            return Helper::failedField('email', __('auth.sign_up_failed'));
        }

        return ['message' => __('auth.sign_up_done')];
    }

    public function resendVerification(EmailFr $fr)
    {
        $user = User::where('email', request('email'))->first();

        if ($user and ! $user->email_verified_at) {
            dispatch(new EmailVerificationJob($user));
        }

        return ['message' => __('auth.resend_done')];
    }

    public function verifyEmail()
    {
        abort_if(! request()->hasValidSignature(), 401, __('auth.verify_signature'));

        $user = User::find(request('id'));

        if (! $user or hash_equals($user->getEmailForVerification(), request('hash'))) {
            abort(401, __('auth.verify_hash'));
        }

        if (! $user->email_verified_at) {
            $user->markEmailAsVerified();
        }

        return redirect(config('front.url'));
    }
}
