<?php

namespace App\Http\Controllers\Auth;

use App\Helper;
use App\Http\Requests\Auth\ResetFr;
use App\Http\Requests\EmailFr;
use App\Jobs\ForgotPasswordJob;
use App\Models\User;

class ResetController
{
    public function forgot(EmailFr $fr)
    {
        $user = User::where('email', request('email'))->first();

        if ($user) {
            ForgotPasswordJob::dispatch($user->email);
        }

        return ['message' => __('auth.forgot_emailed')];
    }

    public function reset(ResetFr $fr)
    {
        $result = \Password::broker()->reset(request()->input(), function ($user, $password) {
            $user->password = $password;
            $user->save();

            $user->tokens()->delete();
        });

        if ($result != \Password::PASSWORD_RESET) {
            return Helper::failedField('email', __('auth.reset_failed'));
        }

        return ['status' => true];
    }
}
