<?php

namespace App\Http\Resources;

use App\Enums\RoleEnum;
use Illuminate\Http\Resources\Json\JsonResource;

/** @mixin \App\Models\User */
class UserResource extends JsonResource
{
    public function resolve($request = null)
    {
        return ! $this->resource ? null : parent::resolve($request);
    }

    public function toArray($request)
    {
        if (! $this->resource) {
            return null;
        }

        return [
            'name' => $this->name,
            'email' => $this->email,
            'role' => data_get($this->roles()->first(), 'name', RoleEnum::MEMBER),
            'verified' => ! is_null($this->email_verified_at),
            'logo' => \Helper::getFileUrl($this->logo),
            'gallery' => \Helper::getFileUrl($this->gallery)
        ];
    }
}
