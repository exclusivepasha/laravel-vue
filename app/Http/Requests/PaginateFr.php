<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use App\Traits\RequestForm;

class PaginateFr extends FormRequest
{
    use RequestForm;

    public function rules()
    {
        return [
            'search' => 'nullable|string',
            'page' => 'nullable|integer',
            'per_page' => 'nullable|integer',
            'order_by' => 'nullable|string',
            'order_dir' => 'nullable|string|in:asc,desc'
        ];
    }

    public function messages()
    {
        return [
            'order_by.*' => __('app.order_by'),
            'order_dir.*' => __('app.order_dir'),
        ];
    }
}
