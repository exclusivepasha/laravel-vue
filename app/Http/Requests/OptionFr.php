<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use App\Traits\RequestForm;

class OptionFr extends FormRequest
{
    use RequestForm;

    public function rules()
    {
        return [
            'id'     => 'required|string|max:250',
            'value'  => 'required'
        ];
    }
}
