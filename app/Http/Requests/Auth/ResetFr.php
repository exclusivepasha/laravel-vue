<?php

namespace App\Http\Requests\Auth;

use App\Traits\RequestForm;
use Illuminate\Foundation\Http\FormRequest;

class ResetFr extends FormRequest
{
    use RequestForm;

    public function rules()
    {
        return [
            'token' => 'required|string',
            'email' => config('validation.email'),
            'password' => config('validation.password_confirmed')
        ];
    }
}
