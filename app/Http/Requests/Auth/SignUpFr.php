<?php

namespace App\Http\Requests\Auth;

use Illuminate\Foundation\Http\FormRequest;
use App\Traits\RequestForm;

class SignUpFr extends FormRequest
{
    use RequestForm;

    public function rules()
    {
        return [
            'email' => config('validation.email_users_unique'),
            'password' => config('validation.password_confirmed'),
            'name' => config('validation.user_name')
        ];
    }
}
