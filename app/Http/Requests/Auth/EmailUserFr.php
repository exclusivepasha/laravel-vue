<?php

namespace App\Http\Requests\Auth;

use Illuminate\Foundation\Http\FormRequest;
use App\Traits\RequestForm;

class EmailUserFr extends FormRequest
{
    use RequestForm;

    public function rules()
    {
        return [
            'email' => config('validation.email_users_unique'),
        ];
    }
}
