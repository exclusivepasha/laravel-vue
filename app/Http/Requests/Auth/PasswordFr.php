<?php

namespace App\Http\Requests\Auth;

use App\Traits\RequestForm;
use Illuminate\Foundation\Http\FormRequest;

class PasswordFr extends FormRequest
{
    use RequestForm;

    public function rules()
    {
        return [
            'current' => config('validation.password'),
            'password' => config('validation.password_confirmed')
        ];
    }
}
