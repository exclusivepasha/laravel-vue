<?php

namespace App\Http\Requests\Auth;

use Illuminate\Foundation\Http\FormRequest;
use App\Traits\RequestForm;

class DataFr extends FormRequest
{
    use RequestForm;

    public function rules()
    {
        return [
            'name' => config('validation.user_name')
        ];
    }
}
