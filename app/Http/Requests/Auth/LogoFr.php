<?php

namespace App\Http\Requests\Auth;

use Illuminate\Foundation\Http\FormRequest;
use App\Traits\RequestForm;

class LogoFr extends FormRequest
{
    use RequestForm;

    public function rules()
    {
        return [
            'logo' => config('validation.image')
        ];
    }
}
