<?php

namespace App\Http\Requests\Auth;

use Illuminate\Foundation\Http\FormRequest;
use App\Traits\RequestForm;

class SignInFr extends FormRequest
{
    use RequestForm;

    public function rules()
    {
        return [
            'email' => config('validation.email'),
            'password' => config('validation.password')
        ];
    }
}
