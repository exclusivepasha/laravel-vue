<?php

namespace App\Http\Requests\Crud;

use Illuminate\Foundation\Http\FormRequest;
use App\Traits\RequestForm;

class PostCrudFr extends FormRequest
{
    use RequestForm;

    public function rules()
    {
        return [
            'content'   => 'required|string',
            'published' => 'nullable|date_format:Y-m-d',
            'title'     => 'required|string|max:250'
        ];
    }
}
