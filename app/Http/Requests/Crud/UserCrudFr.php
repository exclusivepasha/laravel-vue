<?php

namespace App\Http\Requests\Crud;

use Illuminate\Foundation\Http\FormRequest;
use App\Traits\RequestForm;

class UserCrudFr extends FormRequest
{
    use RequestForm;

    public function rules()
    {
        return [
            'email'     => 'required|email:filter|max:150|unique:users,email,'. $this->id,
            'name'      => 'required|string',
            'password'  => 'nullable|string|min:8',
            'role'      => 'required|string'
        ];
    }
}
