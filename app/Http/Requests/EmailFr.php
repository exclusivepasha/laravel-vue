<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use App\Traits\RequestForm;

class EmailFr extends FormRequest
{
    use RequestForm;

    public function rules()
    {
        return [
            'email' => config('validation.email'),
        ];
    }
}
