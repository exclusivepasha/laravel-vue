<?php

namespace App\Http\Middleware;

use App\Models\PersonalAccessToken;
use Closure;
use Illuminate\Http\Request;

class AutoRemoveToken
{
    public function handle(Request $request, Closure $next)
    {
        if ($request->bearerToken()) {
            $token = PersonalAccessToken::findToken($request->bearerToken());

            $token and now()->subDays($token->expiry_days)->isAfter($token->updated_at) and $token->delete();
        }

        return $next($request);
    }
}
