<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;

/**
 * @property int $id
 * @property int $user_id
 * @property string $title
 * @property string $content
 * @property Carbon $created_at
 * @property Carbon $updated_at
 *
 * @mixin Builder
 */

class Post extends Model
{
    protected $fillable = ['title', 'content', 'published'];

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
