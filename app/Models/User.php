<?php

namespace App\Models;

use App\Enums\FileEnum;
use App\Enums\RoleEnum;
use App\Jobs\EmailVerificationJob;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\MorphMany;
use Illuminate\Database\Eloquent\Relations\MorphOne;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;
use Spatie\Permission\Traits\HasRoles;

/**
 * @property int $id
 * @property string $name
 * @property string $email
 * @property string $password
 * @property string $email_verified_at
 * @property Carbon $created_at
 * @property Carbon $updated_at
 *
 * @property MorphOne $logo
 * @property MorphMany $gallery
 * @property HasMany $posts
 *
 * @method Builder role($roles, $guard = null)
 *
 * @mixin Builder
 * @mixin Model
 */
class User extends Authenticatable
{
    use HasApiTokens, HasRoles, Notifiable;

    protected $fillable = ['name', 'email', 'password'];

    protected $hidden = ['password', 'email_verified_at'];

    public function setPasswordAttribute($value)
    {
        $this->attributes['password'] = \Hash::make($value);
    }

    public function gallery()
    {
        return $this->morphMany(File::class, 'morphable')
                    ->where('type', FileEnum::USER_GALLERY);
    }

    public function logo()
    {
        return $this->morphOne(File::class, 'morphable')
                    ->where('type', FileEnum::USER_LOGO);
    }

    public function posts()
    {
        return $this->hasMany(Post::class);
    }

    protected static function boot()
    {
        parent::boot();

        parent::created(function (self $user) {
            $user->assignRole(RoleEnum::MEMBER);

            if (! $user->email_verified_at) {
                dispatch(new EmailVerificationJob($user));
            }
        });
    }

}
