<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;

/**
 * @property string $id
 * @property string $value
 * @property bool $is_json
 * @property bool $init
 *
 * @mixin Builder
 */
class Option extends Model
{
    protected $table = 'options';

    public $timestamps = false;

    protected $keyType = 'string';

    public $incrementing = false;

    protected $fillable = ['id', 'value'];

    protected $hidden = ['is_json', 'init'];

    public function getValueAttribute($value)
    {
        return $this->is_json ? json_decode($value) : $value;
    }

    public function setValueAttribute($value)
    {
        $is_json = is_array($value) || is_object($value);
        $this->attributes['value'] = $is_json ? json_encode($value) : $value;
        $this->attributes['is_json'] = $is_json;
    }

    /** Get all initial options */
    public static function init()
    {
        return self
            ::where('init', 1)
            ->get(['id', 'value', 'is_json'])
            ->pluck('value', 'id');
    }

    public static function getValue($name, $default = null)
    {
        $item = self::find($name);

        return $item ? $item->value : $default;
    }

    /**
     * Get options by `id`
     *
     * @param array $data - array of ids (or part of id with %)
     * @return mixed
     */
    public static function getValues($data)
    {
        return self::where(function ($query) use ($data) {
            foreach ($data as $key) {
                $query->orWhere('id', 'LIKE', $key);
            }
        })->pluck('value', 'id');
    }

    public static function setValues($array)
    {
        foreach ($array as $key => $val) {
            self::updateOrCreate(['id' => $key], ['value' => $val]);
        }
    }
}
