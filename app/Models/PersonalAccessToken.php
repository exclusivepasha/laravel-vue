<?php

namespace App\Models;

use Carbon\Carbon;
use Laravel\Sanctum\PersonalAccessToken as SanctumPAT;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;

/**
 * @property int $id
 * @property string $name
 * @property string $token
 * @property string $abilities
 * @property int $expiry_days
 * @property Carbon $last_used_at
 * @property Carbon $created_at
 * @property Carbon $updated_at
 *
 * @mixin Model
 * @mixin Builder
 */

class PersonalAccessToken extends SanctumPAT
{
    public $incrementing = false;

    protected static function boot()
    {
        parent::boot();

        parent::creating(function (self $token) {
            $token->id = \Str::uuid();
            $token->expiry_days = config('auth.expire.' . (request('remember') ? 'remember' : 'default'));
        });
    }
}
