<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;

/**
 * @property int $id
 * @property string $title
 * @property string $path
 * @property int $type
 * @property Carbon $created_at
 *
 * @mixin Builder
 */

class File extends Model
{
    public $timestamps = false;

    protected $fillable = ['title', 'path', 'type', 'created_at'];

    protected $hidden = ['morphable_id', 'morphable_type'];

    public function morphable()
    {
        return $this->morphTo();
    }

    protected static function boot()
    {
        parent::boot();

        parent::updated(function (File $file) {
            if ($file->path != $file->getOriginal('path')) {
                \Storage::delete($file->getOriginal('path'));
            }
        });

        parent::deleted(function (File $file) {
            \Storage::delete($file->path);
        });
    }

}
