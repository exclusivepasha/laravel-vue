<?php

namespace App\Jobs;

use Illuminate\Auth\Notifications\ResetPassword;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class ForgotPasswordJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $email;

    public function __construct(string $email)
    {
        $this->queue = 'high';
        $this->email = $email;
    }

    public function handle()
    {
        ResetPassword::createUrlUsing(function ($user, $token) {
            return config('front.recovery_url') . "?token={$token}&email={$user->email}";
        });

        \Password::sendResetLink([
            'email' => $this->email
        ]);
    }
}
